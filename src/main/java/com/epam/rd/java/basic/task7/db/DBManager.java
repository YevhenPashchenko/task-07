package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String CONNECTION_URL;
	private static final String APP_PROPS_FILE = "app.properties";
	private static final String INSERT_USER = "INSERT INTO users VALUES(DEFAULT, ?)";
	private static final String FIND_ALL_USERS = "SELECT * FROM users";
	private static final String INSERT_TEAM = "INSERT INTO teams VALUES(DEFAULT, ?)";
	private static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
	private static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES(?, ?)";
	private static final String GET_USER_TEAMS = "SELECT id, name FROM teams t, users_teams ut " +
			"WHERE t.id = ut.team_id AND ut.user_id = ?";
	private static final String DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
	private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
	private static final String DELETE_USER = "DELETE FROM users WHERE id = ?";

	static {
		Properties properties = new Properties();
		try(FileInputStream fileInputStream = new FileInputStream(APP_PROPS_FILE)) {
			properties.load(fileInputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		CONNECTION_URL = properties.getProperty("connection.url");
	}

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public Connection getConnection() throws SQLException {

		return DriverManager.getConnection(CONNECTION_URL);
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try(Statement stmt = DBManager.getInstance().getConnection().createStatement()) {
			ResultSet rs = stmt.executeQuery(FIND_ALL_USERS);
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try(PreparedStatement pstmt = DBManager.getInstance().getConnection().prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setString(1, user.getLogin());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				user.setId(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return user.getId() > 0;
	}

	public boolean deleteUsers(User... users) throws DBException {
		int result = 0;
		try(PreparedStatement pstmt = DBManager.getInstance().getConnection().prepareStatement(DELETE_USER)) {
			for (User user:
				 users) {
				pstmt.setInt(1, user.getId());
				result += pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return result > 0;
	}

	public User getUser(String login) throws DBException {
		for (User user:
			 findAllUsers()) {
			if (login.equals(user.getLogin())) {
				return user;
			}
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		for (Team team:
			 findAllTeams()) {
			if (name.equals(team.getName())) {
				return team;
			}
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try(Statement stmt = DBManager.getInstance().getConnection().createStatement()) {
			ResultSet rs = stmt.executeQuery(FIND_ALL_TEAMS);
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(PreparedStatement pstmt = DBManager.getInstance().getConnection().prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setString(1, team.getName());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				team.setId(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return team.getId() > 0;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		int result = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SET_TEAMS_FOR_USER);
			con.setAutoCommit(false);
			for (Team team:
				 teams) {
				pstmt.setInt(1, user.getId());
				pstmt.setInt(2, team.getId());
				result += pstmt.executeUpdate();
			}
			con.commit();
			con.setAutoCommit(true);
		} catch (SQLException e) {
			if (con != null) {
				try {
					con.rollback();
				} catch (SQLException ex) {
					throw new DBException(ex.getMessage(), ex.getCause());
				}
			}
			throw new DBException(e.getMessage(), e.getCause());
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				throw new DBException(e.getMessage(), e.getCause());
			}
		}
		return result > 0;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try(PreparedStatement pstmt = DBManager.getInstance().getConnection().prepareStatement(GET_USER_TEAMS)) {
			pstmt.setInt(1, user.getId());
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		int result = 0;
		try(PreparedStatement pstmt = DBManager.getInstance().getConnection().prepareStatement(DELETE_TEAM)) {
			pstmt.setString(1, team.getName());
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return result > 0;
	}

	public boolean updateTeam(Team team) throws DBException {
		int result;
		try(PreparedStatement pstmt = DBManager.getInstance().getConnection().prepareStatement(UPDATE_TEAM)) {
			pstmt.setString(1, team.getName());
			pstmt.setInt(2, team.getId());
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return result > 0;
	}

}
